package com.example.ltrojanowski.glidetest;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ltrojanowski on 25.08.2017.
 */

public class ImageCardRecyclerViewAdapter extends RecyclerView.Adapter<ImageCardRecyclerViewAdapter.ViewHolder> {

    private Context mContext;
    private List<ImageWithDescription> mDataset;

    ImageCardRecyclerViewAdapter(Context context) {
        mContext = context;
        mDataset = new ArrayList<>();
    }

    public void setDataset(List<ImageWithDescription> dataset) {
        mDataset.clear();
        mDataset.addAll(dataset);
        this.notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext)
                .inflate(R.layout.image_card, parent, false);

        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ImageWithDescription model = mDataset.get(position);
        holder.textView.setText(model.getDescription());
        Glide.with(mContext)
                .load(model.getImageUrl())
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView image;
        public TextView textView;

        public ViewHolder(View root) {
            super(root);
            image = (ImageView) root.findViewById(R.id.image_image_view);
            textView = (TextView) root.findViewById(R.id.info_text);
        }
    }
}
