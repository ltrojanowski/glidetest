package com.example.ltrojanowski.glidetest;

/**
 * Created by ltrojanowski on 25.08.2017.
 */

public class ImageWithDescription {
    private String imageUrl;
    private String description;

    public ImageWithDescription(String imageUrl, String description) {
        this.imageUrl = imageUrl;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
