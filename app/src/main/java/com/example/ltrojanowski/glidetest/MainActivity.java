package com.example.ltrojanowski.glidetest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private RecyclerView cardsRecyclerView;
    private ImageCardRecyclerViewAdapter cardsRecyclerViewAdapter;
    private RecyclerView.LayoutManager cardsRecyclerViewLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cardsRecyclerView = (RecyclerView) findViewById(R.id.image_cards_recycler_view);

        cardsRecyclerViewLayoutManager = new LinearLayoutManager(this);

        cardsRecyclerViewAdapter = new ImageCardRecyclerViewAdapter(this);
        cardsRecyclerView.setAdapter(cardsRecyclerViewAdapter);
        cardsRecyclerView.setLayoutManager(cardsRecyclerViewLayoutManager);

        cardsRecyclerViewAdapter.setDataset(Arrays.asList(
                new ImageWithDescription("http://goo.gl/gEgYUd", "foo"),
                new ImageWithDescription("https://github.com/bumptech/glide/blob/master/static/glide_logo.png?raw=true", "boo"),
                new ImageWithDescription("http://goo.gl/gEgYUd", "foo"),
                new ImageWithDescription("https://github.com/bumptech/glide/blob/master/static/glide_logo.png?raw=true", "boo"),
                new ImageWithDescription("http://goo.gl/gEgYUd", "foo"),
                new ImageWithDescription("https://github.com/bumptech/glide/blob/master/static/glide_logo.png?raw=true", "boo"),
                new ImageWithDescription("http://goo.gl/gEgYUd", "foo"),
                new ImageWithDescription("https://github.com/bumptech/glide/blob/master/static/glide_logo.png?raw=true", "boo"),
                new ImageWithDescription("http://goo.gl/gEgYUd", "foo"),
                new ImageWithDescription("https://github.com/bumptech/glide/blob/master/static/glide_logo.png?raw=true", "boo"),
                new ImageWithDescription("http://goo.gl/gEgYUd", "foo"),
                new ImageWithDescription("https://github.com/bumptech/glide/blob/master/static/glide_logo.png?raw=true", "boo"),
                new ImageWithDescription("http://goo.gl/gEgYUd", "foo"),
                new ImageWithDescription("https://github.com/bumptech/glide/blob/master/static/glide_logo.png?raw=true", "boo"),
                new ImageWithDescription("http://goo.gl/gEgYUd", "foo"),
                new ImageWithDescription("https://github.com/bumptech/glide/blob/master/static/glide_logo.png?raw=true", "boo"),
                new ImageWithDescription("http://goo.gl/gEgYUd", "foo"),
                new ImageWithDescription("https://github.com/bumptech/glide/blob/master/static/glide_logo.png?raw=true", "boo"),
                new ImageWithDescription("http://goo.gl/gEgYUd", "foo"),
                new ImageWithDescription("https://github.com/bumptech/glide/blob/master/static/glide_logo.png?raw=true", "boo"),
                new ImageWithDescription("http://goo.gl/gEgYUd", "foo"),
                new ImageWithDescription("https://github.com/bumptech/glide/blob/master/static/glide_logo.png?raw=true", "boo"),
                new ImageWithDescription("http://goo.gl/gEgYUd", "foo"),
                new ImageWithDescription("https://github.com/bumptech/glide/blob/master/static/glide_logo.png?raw=true", "boo"),
                new ImageWithDescription("http://goo.gl/gEgYUd", "foo")
        ));

    }
}
